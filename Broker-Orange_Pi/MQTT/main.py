#! /usr/bin/python3
#-*- coding: utf-8 -*-

"""
    DESCRIPCI?N...
    This software is free, licensed y distributed under GPL v3.
    (see COPYING) WITHOUT ANY WARRANTY.
    You can see a description of license here: http://www.gnu.org/copyleft/gpl.html
    Copyright(c) 2017 by fandres "Fabian Salamanca" <fabian.salamanca@openmailbox.org>
                         Marlon...
    Distributed under GPLv3+
    Hardware: Orange Pi Zero
     Stop Alarm(Buzer): [IN]
    Configure wifi and MQTT
"""


__author__ = "Fabian A. Salamanca F."
__copyright__ = "Copyright 2017, Eco-Tec"
__credits__ = ["Fabian A. Salamanca F, Marlon Mauricio Moreno"]
__license__ = "GPL V3.0"
__version__ = "1.4.1"
__maintainer__ = __author__
__email__ = "fabian.salamanca@gmail.com"

# imports hardware
#import OPi.GPIO as GPIO
# imports modulos
from .config import BROKER_IP, USER, PASSWORD
#from config import BROKER
import paho.mqtt.client as mqtt
# imports library python
from time import sleep
import threading
import time
import re
#import queue
from .config import*  # IMPORTAR SOLO LO USADO
import os
#import sys
import pygame

from GUI.source.main import screen

from GUI.source.sonda import sonda

from GUI.source.RGB import *

from GPIO.GPIO import *

from LORA.Lora import *

##########----------    Class   ----------##########


class Archivos ():
    def __init__(self,ruta):
        self.plantilla = ""  # open("plantilla.txt",'r')
        self.file = ""
        self.ruta=ruta

    def crear_file(self, name_file, broker, ubicacion, nodo, sensor, var):
        self.plantilla = open(self.ruta+"/MQTT/plantilla.txt", 'r')
        self.file = open(name_file, 'w')
        self.encabezado(broker, ubicacion, sensor, nodo, var)
        self.file.close()
        self.plantilla.close()

    def encabezado(self, broker, ubicacion, sensor, nodo, var):
        linea = self.plantilla.readline()
        while linea != "":

            if linea == "# Copyrigh\n":
                #m=re.sub("\s", __copyright__ ,linea)
                self.file.write("# " + __copyright__ + "\n")
            elif linea == "# License\n":
                # print("# "+__copyright__+"\n")
                m = re.sub("\n", "  ," + __license__ + "\n", linea)
                # print(m)
                self.file.write(m)
            elif linea == "# Version\n":
                # print("# "+__copyright__+"\n")
                m = re.sub("\n", "  ," + __version__ + "\n", linea)
                # print(m)
                self.file.write(m)
            elif linea == "# Generado por el Broker\n":
                m = re.sub("\n", "  ," + broker + "\n", linea)
                # print(m)
                self.file.write(m)
            elif linea == "# fecha\n":
                m = re.sub("\n", "  ," +
                           time.strftime("%d/%m/%y") + "\n", linea)
                # print(m)
                self.file.write(m)
            elif linea == "# UBICACION\n":
                m = re.sub("\n", "  " + ubicacion + "\n", linea)
                # print(m)
                self.file.write(m)
            elif linea == "# SENSOR\n":
                m = re.sub("\n", "  ," + sensor + "\n", linea)
                # print(m)
                self.file.write(m)
            elif linea == "# NODO\n":
                m = re.sub("\n", "  ," + nodo + "\n", linea)
                # print(m)
                self.file.write(m)
            elif linea == "TIME,var\n":
                m = re.sub("var", var + "\n", linea)
                self.file.write(m)
            else:
                # print(linea)
                self.file.write(linea)
            linea = self.plantilla.readline()

##########----------    Class   ----------##########


class Prediction(object):
    """Algoritmo que permite hasta cierto grado la prediccion"""

    def __init__(self, arg):
        super(Prediction, self).__init__()
        self.arg = arg




##########----------    Class   ----------##########
class BrokerManager(threading.Thread):
    """Clase que gestiona el Broker, la activacion y pausa de la alarma
    Ademas de lavisualizacion de los datos obtenidos y sus estados"""

    def __init__(self, main, ip=BROKER_IP, port=1883):
        threading.Thread.__init__(self)
        self.Client = mqtt.Client()
        self._ip = ip
        self._port = port
        self.Client.on_connect = self.on_connect
        self.Client.on_message = self.on_message
        self.archivos = main.archivos
        self.screen = main.screen
        self.gpio = main.gpio

    def set_ip(self, ip):
        "instancia la Ip usada por el Broker"
        self.ip = ip

    def set_port(self, port):
        "intancia el puerto MQTT"
        self.port = port

    def on_connect(self, client, userdata, flags, rc):
        # print("Connected with result code "+str(rc))  # Debug
        client.subscribe("/#")

    # The callback for when a PUBLISH message is received from the server.
    def on_message(self, client, userdata, msg):
        dato = re.sub("b", "", str(msg.payload))
        dato = re.sub("'", "", str(dato))
        #print("llego "+ dato)
        try:
            #print(msg.topic)
            
            name = self.archivos[msg.topic]
            self.add_dato(name, dato)
            
            try:
                for i in LIST_SENSOR:
                    if i in msg.topic:
                            print(i)
                            if "Temp" in msg.topic:
                                print("temp "+ dato )
                                self.screen.sonda.set_dato_sonda(i,"VAL_TEM",dato)
                                #self.screen.sonda.reset_time(i)
                                if float(dato) < 1:
                                    self.gpio.write('198','1')
                            elif "Hum" in msg.topic:
                                print("hum "+ dato)
                                self.screen.sonda.set_dato_sonda(i,"VAL_HUM",dato)
                #self.screen.update()
                    #if self.screen.display == i:

            except:
                print("No data screen")
                  
        except:
            print("Topico no valido")
        self.screen.update()
            

    def add_dato(self, archivo, dato):
        """"Guarda un nuevo dato, si el archivo no existe lo creea"""
        # print(dato)
        a = open(archivo, 'a')
        a.write(time.strftime("%H:%M:%S") + "," + (dato) + "\n")
        a.close()

    def connect(self):
        self.Client.username_pw_set(USER, password=PASSWORD)
        self.Client.connect(self._ip, self._port, keepalive=15)

    def disconnect(self):
        self.Client.disconnect()
        self.screen.quit()

    def run(self):
        self.Client.loop_forever()
    
    def publish(self, topic, payload):
        self.Client.publish(topic, payload)
        
class Screen():
    """Clase para la gestion de la integración de la interfaz grafica con los datos recividos"""
    def __init__(self,name,path):
        #super(screen, self).__init__()
        os.environ["SDL_FBDEV"] = "/dev/fb0"
        os.environ['SDL_VIDEODRIVER']="fbcon"
        
        pygame.init()
        self.set_size()
        self.set_backcolor()
        
        self.screen = pygame.display.set_mode((320,240))
        self.backcolor=BLACK #es el colo de fondo por defecto para todas las inicializaciones
        self.screen.fill(self.backcolor)
        pygame.mouse.set_visible(False)
        
        self.sonda=sonda(self.screen,path)
        
        self.display=name
        pygame.display.flip()
        #pygame.time.wait(100) 
    def update_time(self):
        self.sonda.set_time()
        self.sonda.set_date(time.strftime('%H:%M'))
        pygame.display.update()
        
    def update(self):
        print("actualizando")
        self.sonda.refresh_all()
        pygame.display.update()
        
    def set_display(self, display):
        try:
            self.sonda.set_sonda(display)
            self.display=display
            self.update()
        except:
            self.display=display
            self.update()
        
    def set_size(self,x=320,y=240):
        self.screen = pygame.display.set_mode((x,y))
        
    def set_backcolor(self, backcolor=BLACK):
        self.backcolor=backcolor        #es el colo de fondo por defecto para todas las inicializaciones
    
    def quit(self):
        pygame.quit()
                
    def add_sonda(self, name=""):
        self.sonda.add_sonda(name)

class main():
    def __init__(self, ruta):
        #Archivos csv -------------------------
        self.ruta = ruta +"/MQTT/data/"
        self.imagenes = ruta + "/GUI/imagenes"
        self.csv = Archivos(ruta)
        self.archivos = {}
        #--------------------------------------
        
        #LORA----------------------------------
        self.lora = Lora ("/dev/ttyS2",115200,timeout=0.1)
        #self.lora.open_port()
        #--------------------------------------
        
        #Gpio, alarma -------------------------
        self.gpio = GPIO()
        self.gpio.export("198")
        #print (self.gpio.read("198"))
        self.gpio.direction("198","out")
        self.gpio.write('198','0')
        #--------------------------------------
        #Interfaz grafica ---------------------
        self.screen = Screen("",self.imagenes)
        #self.screen.sonda.set_path(self.imagenes)
        #--------------------------------------
        #Broker--------------------------------
        self.BrokerManager = BrokerManager(self, ip=BROKER_IP, port=1883)
        self.BrokerManager.connect()
        self.BrokerManager.start()  # inicia el hilo
        #--------------------------------------

        #self.hilo_padre = {}
        #self.hilo_hijo = {}
        self.list_topic = {}
        self.generate_topic()
        # print(self.archivos)
        #print(self.list_topic)
        #print(self.screen.sondas)
        list_sondas=[]
        for i,j in self.screen.sonda.sondas.items():
            list_sondas.append(i)
        self.screen.set_display(list_sondas[0])
        # for i in self.archivos:
        #    print("key "+  str(i)+ "  "+"value " + str(self.archivos[i]))
        print("Modo infinito")
        a=1 #Determina la sonda a mostrar
        B=0 #Determina el tiempo entre sondas
        c=0 #Determina el tiempo que dura encendido la alarma
        while True:
            for i in list_sondas:
                self.screen.sonda.add_time(i)                
            sleep(1)
            self.screen.update_time()
            if B==10:
                self.screen.set_display(list_sondas[a])
                B=0
                a=a+1
                if a==len(list_sondas):
                    a=0
            if self.gpio.input('198')==1:
                c=c+1
            if c==5:
                self.gpio.write('198','0')
                c=0
            B=B+1
            ##Comentar estas lineas si no se utiliza el lora -----------------------------------
#            mm=self.lora.read()
            
            
            #if mm[0] != '':
                #print(mm)
                #print(mm[0])
                #self.BrokerManager.publish("/UVAS/Hum/Dth22/SONDA_SUR/SENSOR_1",mm[1])
                #self.BrokerManager.publish("/UVAS/Temp/Dth22/SONDA_SUR/SENSOR_1",mm[0])
            ##------------------------------------------------------------------------------------    
            #print("halo")
            #self.BrokerManager.publish("/UVAS/Temp/Dth22/SONDA_SUR/SENSOR_1","23")
            
        self.BrokerManager.disconnect()
        self.lora.closed_port()

    def generate_topic(self):
        print(LIST_SENSOR)
        c = 0
        f = "/" + CULTIVO["TIPO_CULTIVO"]
        for i in LIST_SENSOR:
            # print("a"+i)
            #print(i)
            self.screen.add_sonda(i)
            for m in LIST_SENSOR[i]:
               # print(m)
                if "SENSOR" in m:
                    for n in LIST_SENSOR[i][m]:
                        topic = f + \
                            str(LIST_SENSOR[i][m][n][0]) + "/" + i + "/" + m
                        self.list_topic["topic_" + str(c)] = topic
                        name = str(LIST_SENSOR[i][m][n][1]) + "/" + i + "/" + m
                        name = re.sub("/", "_", name)
                        self.add_file(name, topic, LIST_SENSOR[i][m][n][1], "," + LIST_SENSOR[i]["LOCATION"], i, m)
                        c = c + 1

    def add_file(self, name, topic, var, location, nodo, sensor):
        """Crea un archivo nuevo y lo agrega al diccionario """
        name = self.ruta + str(name) + "_" + \
            time.strftime("%d_%m_%y") + "_" + ".csv"
        self.archivos[topic] = name
        #print(name)
        self.csv.crear_file(name, BROKER["NUMBER_BROKER"], location, nodo, sensor, var)


    # def hilos(self):
    #    pass


##########----------    Main   ----------##########
if __name__ == '__main__':
    main = main(os.getcwd())
