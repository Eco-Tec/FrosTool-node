﻿## FrosTool
Herramienta de monitoreo de variables climaticas para la generación de alertas tempranas para mitiga los efectos de las heladas en los cultivos del departamento de Boyaca- Col

# Arbol de directorios
- 3D-Model: Diseños de lo relacionado con las sondas, cajas, soportes, etc.
- Articulo: Contiene un Articulo cort ode investigación enformato IEEE.
- Docs: Contiene la documentación o linkeos hacia la misma (Texto, links, Imagenes)
- Hardware: Referente al hardware desarrollado (Esquemas, PCBs)
- Images: Aqui colocar imagenes solamente referente a el desarrollo del proyecto.
- Software: Principalmente el codigo desarrollado para el proeycto.

# License
...

Este Proyecto
- @fandres323
- @maurinc2010
